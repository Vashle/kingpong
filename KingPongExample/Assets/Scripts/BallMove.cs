﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMove : MonoBehaviour {
    public float moveSpeed;
    Rigidbody2D rb;
    SpriteRenderer sr;
    Vector3 startPos;
    public int myOwner; //who's the last paddle you touched?

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        startPos = transform.position;
        //we can change the way the ball moves at start:
        ResetBall();
        //ResetBall();
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 ballPos = transform.position;
        //attempting a bug fix for out of bounds balls
        if (ballPos.y > 5.5 || ballPos.y < -5.5)
            ResetBall();


        //move the ball every frame:
        //rb.MovePosition(transform.position + moveVel);
	}
    //from https://noobtuts.com/unity/2d-pong-game
    //we want to check where on the racket the ball is hitting.
    float HitFactor(Vector2 ballPos, Vector2 racketPos, float racketHeight)
    {
        //1 = at top of racket
        //0 = at mid of racket
        //-1 = at bottom of racket
        return (ballPos.y - racketPos.y) / racketHeight;
    }
    //this is called the frame that the object's collider hits anoter collider:
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.CompareTag("Goal"))
        {
            if (myOwner == 1)
            {
                GameManager.GM.p1Score++;
                UIManager.UIM.P1ScoreUpdate(GameManager.GM.p1Score);
            }
            else
            {
                GameManager.GM.p2Score++;
                UIManager.UIM.P2ScoreUpdate(GameManager.GM.p2Score);
            }

            ResetBall();
        }
        //if (other.transform.CompareTag("TopBottom"))
        //{
        //    moveVel.y *= -1;
        //}
        if (other.transform.CompareTag("Player"))
        {
            Vector2 dir;//this has to be declared up here so we can use it outside of the if.
            myOwner = other.transform.GetComponent<PaddleMove>().playerNum;
            float y = HitFactor(transform.position,
                other.transform.position,
                other.collider.bounds.size.y);

            if (myOwner == 1)//if you hit player one:
            {
                dir = new Vector2(1, y).normalized;
            }
            else
            {
                dir = new Vector2(-1, y).normalized;
            }
            rb.velocity = dir * moveSpeed;
            ChangeColor();
            //moveVel.x *= -1;
        }


    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.CompareTag("Building"))
        {
            Vector2 dir;//this has to be declared up here so we can use it outside of the if.
            
            float y = HitFactor(transform.position,
                other.transform.position,
                other.GetComponent<Collider2D>().bounds.size.y);

            //if move left, then right, vice versa


            if (rb.velocity.x < 0)//if you hit player one:
            {
                dir = new Vector2(1, y).normalized;
            }
            else
            {
                dir = new Vector2(-1, y).normalized;
            }
            rb.velocity = dir * moveSpeed;
            //moveVel.x *= -1;
        }
    }

    public void ChangeColor()
    {
        if (myOwner == 1)
        {
            sr.color = Color.white;
        }
        else
        {
            sr.color = Color.green;
        }
    }

    public void ResetBall()
    {
        //stop vel
        rb.velocity = Vector2.zero;

        //set ball to middle of field.
        transform.position = startPos;

        //Start countdown:

        Vector2 v = Vector2.zero;
        //flip a coin to go left or right.

        int coinResult = Random.Range(0, 2);
        //print("the coin says " + coinResult);
        //print("modifier = " + moveModifier);

        if (!GameManager.GM.losersBall)
        {
            if (coinResult == 1)
            {
                v = PickAnAngle("right");

                myOwner = 1;
            }
            else
            {
                //go left
                v = PickAnAngle("left");

                myOwner = 2;
            }
        }
        else
        {
            if (myOwner == 1) //if player1 just scored, go left (towards player 1)
            {
                v = PickAnAngle("left");

                myOwner = 2;
            }
            else
            {
                v = PickAnAngle("right");

                myOwner = 1;
            }
        }
        rb.velocity = v*moveSpeed;
        //print(v);

        ChangeColor();
    }

    //Vector2 PickAnAngle(string leftOrRight)
    //failed radian math:
    //{
    //    Vector2 newAng = Vector2.zero;
    //    float angle = 0;
    //    if(leftOrRight == "right")
    //    {
    //        angle = Random.Range(Mathf.PI / 6, (11 * Mathf.PI) / 6);
    //    }
    //    else
    //    {
    //        angle = Random.Range((5*Mathf.PI) / 6, (7 * Mathf.PI) / 6);

    //    }
    //    newAng.x = Mathf.Cos(angle) * moveSpeed;
    //    newAng.y = Mathf.Sin(angle) * moveSpeed;
    //    print("great pi math = " + newAng);
    //    return newAng;
    //}
    Vector3 PickAnAngle(string leftOrRight)
    {
        Vector3 newAng = Vector3.zero;
        if(leftOrRight == "right")
        {
            newAng = Quaternion.AngleAxis(Random.Range(360.0f, 390.0f), Vector3.forward) * Vector3.right;
        }
        else
        {
            newAng = Quaternion.AngleAxis(Random.Range(150f, 210f), Vector3.forward) * Vector3.right;

        }
        //print("great degrees math = " + newAng);
        return newAng;
    }
}
