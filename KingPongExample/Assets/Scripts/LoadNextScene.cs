﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class LoadNextScene : MonoBehaviour {

    [SerializeField ]
    public VideoPlayer VideoPlayer;
    public string SceneName;

    void Start()
    {
        VideoPlayer.loopPointReached += LoadScene;
    }
    
    void LoadScene(VideoPlayer vp)
    {
        SceneManager.LoadScene(SceneName);
    }
}
