﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class Player : MonoBehaviour {
    public float moveSpeed;
    Rigidbody2D rb;
    SpriteRenderer sr;

    public int playerNum, gold;
    public bool isStunned, isBuilding, canBuild;
    public float stunTime, stunTimeOG;

    Color ogColor;

    //powerup vars:
    //when you pickup a powerup, store its enum into a string:
    public string leftPowerup, rightPowerup;
    public bool hasArmor;

    //building vars:
    BuildingManager bm;
    public Vector2 buildingSize, originalSize;
    Vector2 buildPos; //this should be set each time you run onto a plot.
    public int building1Cost, building2Cost;
    public float building1Time, building2Time;
    GameObject building1, building2;

    public InputDevice Device { get; set; }

    // Use this for initialization
    void Start () {
        stunTime = stunTimeOG;
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        if (GameObject.Find("BuildingManager") != null)
        {
            GetBuildingInfo();
        }
        ogColor = sr.color;
        originalSize = transform.localScale;
        buildingSize = originalSize / 2;
        //Device.GetControl(InputControlType.LeftTrigger);
    }

    // Update is called once per frame
    void Update () {

        //launch the game from the Join menu:
        if (UIManager.UIM.readyToLaunch)
        {
            if (Device.Command.IsPressed)
            {
                GameManager.GM.GetComponent<ChangeScene>().changeScene("SampleScene");
                UIManager.UIM.EnableCanvas(500);
            }
        }
       



        if (building1 == null && GameObject.Find("BuildingManager") != null)
        {
            GetBuildingInfo();
        }


        //when stunned, tick down:
        if (isStunned)
        {
            stunTime -= Time.deltaTime;
            if (stunTime <= 0)
            {
                stunTime = stunTimeOG;
                GetComponent<BoxCollider2D>().enabled = true; //!! this doesn't take invincibility frames into account.
                sr.color = ogColor;
                transform.localScale = originalSize; //if you were stunned while building.
                isStunned = false;
            }
        }

       
        //if you can build and you press down a button
        //! note: right now this is working with button press.  if we want to change it to button hold, we can, but it'll be a bit more of a pain.
        if(canBuild && Device.Action1.WasPressed)
        {
            if (gold >= building1Cost)
            {
                isBuilding = true;
                transform.localScale = buildingSize; //this is probably temp until we have an animation that shows;
                //call the build function on a delay according to b1Time;
                StartCoroutine(Build(building1, building1Time));
            }
            else
            {
                //!! Show some audio or UI cue that you don't have enough gold:
                print("you have not enough minerals");
            }

        }
        if (canBuild && Device.Action2.WasPressed)
        {
            if (gold >= building1Cost)
            {
                isBuilding = true;
                transform.localScale = buildingSize; //this is probably temp until we have an animation that shows;
                //call the build function on a delay according to b1Time;
                StartCoroutine(Build(building2, building2Time));
            }
            else
            {
                //!! Show some audio or UI cue that you don't have enough gold:
                print("you have not enough minerals");
            }

        }

        if (!isStunned && !isBuilding)
        {
            //movement
            Vector3 moveVel = Vector3.zero;

            moveVel.y = Device.LeftStickY * moveSpeed;
            moveVel.x = Device.LeftStickX * moveSpeed;

            rb.MovePosition(transform.position + moveVel);
        }

        /**XBOX CONTROLLER EQUIVALENTS FOR INPUT
         * Action1 = A
         * Action2 = B
         * Action3 = X
         * Action4 = Y
         */

        //Coding this assuming Action3 (X) correlates to leftPowerup
        //'X' WasPressed
        if (Device.Action3.WasPressed)  
        {
            //if player is stunned(and has energydrink), use energydrink
            if(isStunned && leftPowerup.Equals(PowerupHelper.PowerupType.EnergyDrink))
            {
                 stunTime = 0;
                 leftPowerup = null;
                //remove from UI
                isStunned = false;
            }
//TO DO: Implement
            else if(!isStunned)
            {
                /**
                 * [0] BallBoost: Ball move speed up (until ball reset)
                 * [1] BallSlow: Ball move speed down (until ball reset)
                 * [2] Bandaid: +1 health to a building (only on damaged; 3 repairs per building max)
                 * [3] Armor: prevents 1 stun on player (ball, banana, or negates 1 DC building)
                 * [4] EnergyDrink: 1 free un-stun, use-able anytime (ball, banana, or negates 1 DC building)
                 * [5] Pickaxe: Next building building speed x2
                 * [6] ExtraBall: 1 extra ball @ opponent (despawns after score)
                 */
                 switch(leftPowerup)
                 {
                    case "BallBoost":
                        //changes current scene's ball(s) move speed to be up until the balls reset
                        //probably most helpful to be a boolean?

                        //remove from UI
                        break;

                    case "BallSlow":
                        //same as Boost, except slows
                        break;

                    case "Bandaid":
                        //this gets nearest building using plothelper(?)
                        //gets on top of building
                        //gives building +1 hp (building sprite has to change accordingly, 
                        //building script may be better to manage this

            
                        //remove from UI
                        break;

                    case "Armor":
                        //sets specific bool wearingArmor = true
                        //(where stunning is determined), 
                        //if (wearingArmor) then next stun is 'deflected' and player loses armor
                        StartCoroutine(EnableShield(3f));
                        //change player color/sprite to indicate armor?
                        //remove from UI
                        break;

                    case "Pickaxe":
                        //basically does same as Armor, where building checks for bool
                        //if true, speeds up, then changes to false (and removes player's held pickaxe)

                        //give player sprite a pickaxe to hold?
                        //remove from UI

                        //NOTE: that removing this from UI gives players chance to hold 3 powerups (technically)
                        //this may be bad. eek.
                        break;

                    case "ExtraBall":
                        //different from 2ndBall building; this ball is one-time use, then disappears
                        //makes a second ball spawn at opponent

                        //remove from UI
                        break;
                 }
            }
        }

        //Coding this assuming Action4 (Y) correlates to rightPowerup
        // 'Y' WasPressed
        if (Device.Action4.WasPressed)
        {
            if(isStunned &&  rightPowerup.Equals(PowerupHelper.PowerupType.EnergyDrink))
            {
                stunTime = 0;
                rightPowerup = null;
                isStunned = false;
            }
            else if(!isStunned)
            {

            }
        }

    }

    //the trigger will be to check if the player is hit by the ball.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.CompareTag("Ball"))
        {
            AudioManager.AM.PlayClip(4);
            isStunned = true;
            //!!we shouldalso cancel building (if possible)
            //StopAllCoroutines();
            sr.color = Color.red;
            //!! this mostly works but sometimes it will send the ball flying.  we probably need a better solution.
            GetComponent<BoxCollider2D>().enabled = false; //disable your box collider so the ball goes through you?
            //cancel building:
            if (isBuilding)
            {
                isBuilding = false;

                StopCoroutine("Build");
            }
            //!! should also spawn some stars above your head for a visual cue:

            //!! and play a sound effect that:

        }

        if (collision.CompareTag("Gold"))
        {
            AudioManager.AM.PlayClip(0);
            gold += collision.GetComponent<MoneyScript>().amountToGive;
            Destroy(collision.gameObject);
        }

        if (collision.CompareTag("Powerup"))
        {   
            //put into player inventory, if X is full then put in Y, if not; don't destroy
            if (leftPowerup == null)
            {
                leftPowerup = collision.GetComponent<PowerupHelper>().powerUpType.ToString();

                //Prof.'s UI code START
                Sprite pUpSprite = collision.GetComponent<SpriteRenderer>().sprite;

                //calling a UIM function to slot in the sprite:
                UIManager.UIM.SlotPickupSprite(pUpSprite, playerNum);

                //here we can get the powerup type and [do something] if necessary:
                print("just picked up a " + collision.GetComponent<PowerupHelper>().powerUpType.ToString());
                //Prof.'s UI code END

                Destroy(collision.gameObject);
            }
            else if (rightPowerup == null)
            { 
                rightPowerup = collision.GetComponent<PowerupHelper>().powerUpType.ToString();

                //Prof.'s UI code START
                Sprite pUpSprite = collision.GetComponent<SpriteRenderer>().sprite;

                //calling a UIM function to slot in the sprite:
                UIManager.UIM.SlotPickupSprite(pUpSprite, playerNum);

                //here we can get the powerup type and [do something] if necessary:
                print("just picked up a " + collision.GetComponent<PowerupHelper>().powerUpType.ToString());
                //Prof.'s UI code END

                Destroy(collision.gameObject);
            }
            //otherwise, do nothing haha
        }
    }

    //the onTriggerStay function will check to see if player is staying on a building Plot:
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Plot") && !collision.GetComponent<PlotHelper>().isOccupied)
        {
            canBuild = true;
            Color plotColor = collision.GetComponent<SpriteRenderer>().color;
            collision.GetComponent<SpriteRenderer>().color = new Color(plotColor.r, plotColor.g, plotColor.b, 1);
            buildPos = collision.transform.position;
        }
        else
        {
            canBuild = false;
        }
    }

    //making double sure this gets turned off:
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Plot"))
        {
            Color plotColor = collision.GetComponent<SpriteRenderer>().color;
            collision.GetComponent<SpriteRenderer>().color = new Color(plotColor.r, plotColor.g, plotColor.b, .5f);
            canBuild = false;
        }
    }

    void GetBuildingInfo()
    {
        bm = GameObject.Find("BuildingManager").GetComponent<BuildingManager>();
        building1 = bm.buildings[GameManager.GM.chosenBuildings[0]];
        building1Cost = building1.GetComponent<Building>().goldCost;
        building1Time = building1.GetComponent<Building>().buildTime;
        building2 = bm.buildings[GameManager.GM.chosenBuildings[1]];
        building2Cost = building2.GetComponent<Building>().goldCost;
        building2Time = building2.GetComponent<Building>().buildTime;
    }

    IEnumerator EnableShield(float howLong)
    {
        //first off, set hasArmor to be true
        hasArmor = true;
        //also turn on the armor sprite:

        yield return new WaitForSeconds(howLong);

        hasArmor = false;
        //turn off armor sprite:

    }

    IEnumerator Build(GameObject whichBuilding, float delay)
    {
        yield return new WaitForSeconds(delay);
        GameObject go = Instantiate(whichBuilding, buildPos, Quaternion.identity);
        //if it's a graveyard, you have to tell it who spawned it so the skelly walks the right direction:
        if (go.GetComponent<GraveyardControl>() != null)
        {
            go.GetComponent<GraveyardControl>().myPlayer = playerNum;
        }
        gold -= building1Cost;
        transform.localScale = originalSize;
        //allow the player to move again.
        isBuilding = false;
    }


    
}
