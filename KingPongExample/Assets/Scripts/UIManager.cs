﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/*
//Attributions:
    https://commons.wikimedia.org/wiki/File:Xbox_Left_stick_button.svg - CC Share Alike 4.0 by VictorPines


*/

public class UIManager : MonoBehaviour {
    public static UIManager UIM;

    public Text p1ScoreTxt, p2ScoreTxt;
    public Image pauseImg, rPowerup1, rPowerup2, lPowerup1, lPowerup2;
    public GameObject inGameCan, titleCan, countdownPanel, wipePanel, goText; //note, i'm assigning countdown panel in inspector so it doesn't start on.  shouldn't cause any trouble...
    public GameObject[] titleScenePanels; //this will include title screen, mode screen, join screen
    public Text p1Join, p2Join;
    public GameObject p1Controls, p2Controls; //a set of text, etc to explain controls.
    public int currentPanelIndex; //to keep track more easily so we don't have to do System.Array.IndexOf every time.
    public bool countingDown; //so other scripts know not to do anything while this is counting down.
    public bool readyToLaunch; //check from player script.  Much of this is pretty basic/messy but it should work.
    
    
    //when it loads, it loads UI script
    private void Awake()
    {
        if(UIM == null)
        {
            DontDestroyOnLoad(this);
            UIM = this;
        }

        else
        {
            //otherwise destroy itself
            Destroy(gameObject);
        }
       
    }

    // Use this for initialization
    void Start () {
        //loop through all of my children and assign the UI variables:
        foreach(Transform t in GetComponentsInChildren<Transform>())
        {
            switch (t.name)
            {

                case "InGameCanvas":
                    inGameCan = t.gameObject;
                    break;

                case "TitleScreenCanvas":
                    titleCan = t.gameObject;
                    break;

                case "P1Score":
                    p1ScoreTxt = t.GetComponent<Text>();
                    break;

                case "P2Score":
                    p2ScoreTxt = t.GetComponent<Text>();
                    break;

                case "PauseImage":
                    pauseImg = t.GetComponent<Image>();
                    break;

                case "1LPowerup":
                    lPowerup1 = t.GetComponent<Image>();
                    break;

                case "1RPowerup":
                    rPowerup1 = t.GetComponent<Image>();
                    break;
            }
        }


        //turn off some UI stuff we don't need at start:
        TurnOffStartUI();
        if (GameManager.GM.currentScene == 0)
        {
            //turn off ingame can if you're starting from the title.
            inGameCan.SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update () {

        //if (Input.GetKeyDown(KeyCode.Z))
        //{
        //    StartCoroutine(TurnOnWipePanel());
        //}

        //if you're on the title and at the join screen, listen for both players joining
        if (titleCan.activeSelf && currentPanelIndex == 1)
        {
            if (GameManager.GM.playerManager.playerPaddles.Count == 2)
            {
                if(!goText.activeSelf)
                    goText.SetActive(true);
                readyToLaunch = true;

                //the player script will be able to press start to load into the game scene.
            }
            else
            {
                readyToLaunch = false;
                if (goText.activeSelf)
                    goText.SetActive(false);
                
            }
        }
        else
        {
            readyToLaunch = false;
        }

	}


    public void GetJoinScreenUI()
    {
        foreach (Transform t in titleCan.GetComponentsInChildren<Transform>())
        {
            switch (t.name)
            {

                case "P1JoinedText":
                    p1Join = t.GetComponent<Text>();
                    break;

                case "P2JoinedText":
                    p2Join = t.GetComponent<Text>();
                    break;
            }
        }
    }
    public void SlotPickupSprite(Sprite pickupSprite, int playerNum)
    {
        //if the first player is sending it in, check the first player's powerup UI and assign:
        if (playerNum == 1)
        {
            if (!lPowerup1.enabled)
            {
                lPowerup1.sprite = pickupSprite;
                lPowerup1.enabled = true;
            }
            else
            {
                if (!rPowerup1.enabled)
                {
                    rPowerup1.sprite = pickupSprite;
                    rPowerup1.enabled = true;

                }
                else
                {
                    //if the right powerup is full, send a message that you're full:
                    print("full of powerups");
                }
            }

        }

        //!must do same for p2:

    }

    //0 for left, 1 for right
    public void SlotRemoveSprite(int playerNum, int slotnum)
    {
        if (playerNum == 1)
        {
            if (slotnum == 0)
            {
                lPowerup1.enabled = false;
            }
            else
            {
                if (slotnum == 1)
                {
                    rPowerup1.enabled = false;

                }
            }
        }

        //do same for p2
    }


    void TurnOffStartUI()
    {
        pauseImg.enabled = false;
        rPowerup1.enabled = false;
        //rPowerup2.enabled = false;
        lPowerup1.enabled = false;
        //lPowerup2.enabled = false;
    }


    /// <summary>
    /// 0=title;
    /// 1=join
    /// </summary>
    /// <param name="whatPanel"></param>
    public void ActivatePanel(int whatPanel)
    {
        //! this should probably incorporate the wipe animations.
        titleScenePanels[whatPanel].SetActive(true);
        currentPanelIndex = whatPanel;
        //if turning on joinScreen, store the text vars, etc:
        if (whatPanel == 1)
        {
            GetJoinScreenUI();
            EnableJoinElements(true); //turn on the "Press any button" text, ex.
        }
        //turning on before turning off so we don't have a blank screen.
        //that should be fixed with a wipe that will hide panel and scene transitions.
        for (int i = 0; i < titleScenePanels.Length; i++)
        {
            if(i!=whatPanel)
            titleScenePanels[i].SetActive(false);
        }
    }

    public void EnableJoinElements(bool trueOrOff)
    {

        p1Join.gameObject.SetActive(trueOrOff);
        p2Join.gameObject.SetActive(trueOrOff);

    }

    public void EnableJoinElements(int playerNum,bool trueOrOff)
    {
        switch (playerNum)
        {
            case 1:
                p1Join.gameObject.SetActive(trueOrOff);
                p1Controls.SetActive(!trueOrOff);

                break;

            case 2:
                p2Join.gameObject.SetActive(trueOrOff);
                p2Controls.SetActive(!trueOrOff);

                break;
        }

    }



    public void EnableCanvas(int zeroIsTitle)
    {
        //for now we only have two canvases - title or in game.
        //we may add more later or we can probably just deal with this number and have sub-panels for the title canvas for options, etc.
        titleCan.SetActive(false);
        inGameCan.SetActive(false);
        if (zeroIsTitle==0)
        {
            titleCan.SetActive(true);
        }
        else
        {
            inGameCan.SetActive(true);
            //if you enable the ingamecanvas, also enable the countdown timer:
            StartCoroutine(EnableCountdownTimer());
        }
    }

    public IEnumerator TurnOnWipePanel()
    {
        wipePanel.SetActive(true);
        //get the length of the animation (so we can change it variably)
        //from: https://answers.unity.com/questions/692593/get-animation-clip-length-using-animator.html
        Animator anim = wipePanel.GetComponent<Animator>();
        //!!not working atm.  taking a break.
        int randWipe = Random.Range(0, anim.runtimeAnimatorController.animationClips.Length);
        //print(anim.runtimeAnimatorController.animationClips.Length);
        anim.SetInteger("WipeList", randWipe); //this is choosing a random wipe
        float time = 3f;
        RuntimeAnimatorController ac = anim.runtimeAnimatorController;
        for (int i = 0; i < ac.animationClips.Length; i++)
        {
            if (ac.animationClips[i].name == "WipeAnim" + randWipe)
            {
                time = ac.animationClips[i].length;
            }
        }
        //wait until the wipe in is done, then change scene and move to wipe out

        yield return new WaitForSeconds(time);
        wipePanel.SetActive(false);
    }

    public IEnumerator EnableCountdownTimer()
    {
        countingDown = true;
        countdownPanel.SetActive(true);
        //get the length of the animation (so we can change it variably)
        //from: https://answers.unity.com/questions/692593/get-animation-clip-length-using-animator.html
        Animator anim = countdownPanel.GetComponent<Animator>();
        float time = 0f;
        RuntimeAnimatorController ac = anim.runtimeAnimatorController;
        for(int i=0; i<ac.animationClips.Length; i++)
        {
            if(ac.animationClips[i].name == "CountdownAnim")
            {
                time = ac.animationClips[i].length;
            }
        }
        //then wait until the anim is over before turning the panel off

        yield return new WaitForSeconds(time);
        countdownPanel.SetActive(false);
        countingDown = false;
    }

    public void P1ScoreUpdate(int whatScore)
    {
        p1ScoreTxt.text = "Score: " + whatScore;
    }

    public void P2ScoreUpdate(int whatScore)
    {
        p2ScoreTxt.text = "Score: " + whatScore;
    }
}
