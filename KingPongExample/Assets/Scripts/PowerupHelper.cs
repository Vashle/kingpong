﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupHelper : MonoBehaviour {
    //add powerup types here:
    public enum PowerupType { BallBoost, BallSlow, Bandaid, Armor, EnergyDrink, Pickaxe, ExtraBall}
    /**
     * [0] BallBoost: Ball move speed up (until ball reset)
     * [1] BallSlow: Ball move speed down (until ball reset)
     * [2] Bandaid: +1 health to a building (only on damaged; 3 repairs per building max)
     * [3] Armor: prevents 1 stun on player (ball, banana, or negates 1 DC building)
     * [4] EnergyDrink: 1 free un-stun, use-able anytime (ball, banana, or negates 1 DC building)
     * [5] Pickaxe: Next building building speed x2
     * [6] ExtraBall: 1 extra ball @ opponent (despawns after score)
     */

    //use the dropdown in the inspector to set this:
    public PowerupType powerUpType;
	
    // Use this for initialization
	void Start () {
		//Keeping start() around so maybe we want each powerup to drop
        //a particle effect when it comes into existence or something.
	}
}
