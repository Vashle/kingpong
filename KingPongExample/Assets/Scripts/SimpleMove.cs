﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/**
 * Used for simple movement, like the Graveyard Building Type's spawn object. 
 */
public class SimpleMove : MonoBehaviour {
    public float moveSpeed;
    public float moveTime, moveTimeOG;
    public bool isMoving;
    public int myPlayer;

    private void Start()
    {
        //this is a hacky fix and will probably only work for 2 players.
        //when the skeleton spawns, it needs to know if it belongs to P1 or P2.
        //if P2, then look towards the left and move towards the left:
        if (myPlayer == 2)
        {
            GetComponent<SpriteRenderer>().flipX = false;
            moveSpeed *= -1;
        }
    }


    // Update is called once per frame
    void Update () {
        moveTime -= Time.deltaTime;
        if (moveTime <= 0)
        {
            if (isMoving)
            {
                isMoving = false;
            }
            else
            {
                isMoving = true;
            }
            moveTime = moveTimeOG;
        }

        if (isMoving)
        {
            GetComponent<Rigidbody2D>().MovePosition(transform.position + new Vector3(moveSpeed, 0, 0));
        }
	}
}
