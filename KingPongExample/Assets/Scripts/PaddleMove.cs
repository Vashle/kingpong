﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class PaddleMove : MonoBehaviour {
    public float moveSpeed;
    Rigidbody2D rb;
    public int playerNum;
    public InputDevice Device { get; set; }
    private InputControl LT, LB, RT, RB;

    // Use this for initialization
    void Start () {
        //get the rigidbody and store it in our rb var:
        rb = GetComponent<Rigidbody2D>();

        LT = Device.GetControl(InputControlType.LeftTrigger);
        LB = Device.GetControl(InputControlType.LeftBumper);
        RT = Device.GetControl(InputControlType.RightTrigger);
        RB = Device.GetControl(InputControlType.RightBumper);
    }
	
	// Update is called once per frame
	void Update () {
        //make a new vector2 each frame:
        Vector3 moveVel = Vector3.zero;

        //moveVel.y = Input.GetAxis("Vertical" +playerNum) * moveSpeed;
        //moveVel.y = Device.LeftStickY * moveSpeed; //old code for paddle controlled by analog stick



        if (RT.IsPressed || RB.IsPressed) //left trigger & button = down
            moveVel.y -= moveSpeed;

        if (LT.IsPressed || LB.IsPressed) //right trigger & button = up
            moveVel.y += moveSpeed;

        //at the end of update, move the paddle:
        rb.MovePosition(transform.position + moveVel);

	}
}
