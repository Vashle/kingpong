﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building: MonoBehaviour {
    public float buildTime;
    public int goldCost;
    public int healthPoints = 2;
    PlotHelper myPlot;
    public Sprite[] sprites = new Sprite[3];
    SpriteRenderer spriteRenderer;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        //this may not work but trying it here anyways:
        //it works but requires the buildings to have kinematic rbs on them.  I don't think that's a big deal...
        if (other.CompareTag("Plot"))
        {
            other.GetComponent<PlotHelper>().isOccupied = true;
            myPlot = other.GetComponent<PlotHelper>();
        }

        if (other.transform.CompareTag("Ball"))
        {
            TakeDamage(1);
        }
    }
    
    public void TakeDamage(int num)
    {
        healthPoints -= num;
        spriteRenderer.sprite = sprites[healthPoints];
        if (healthPoints <= 0)
        {
            //also drop a puff of smoke anim or something:
            Color plotColor = myPlot.GetComponent<SpriteRenderer>().color;
            myPlot.GetComponent<SpriteRenderer>().color = new Color(plotColor.r, plotColor.b, plotColor.g, .5f); //make it transparent when you die.
            myPlot.isOccupied = false;
            Destroy(gameObject);
        }
    }

    //no go, apparently doesn't fire when destroyed:
    //void OnTriggerExit2D(Collider2D other)
    //{
    //    //AGAIN, this may not work but trying it here anyways:
    //    if (other.CompareTag("Plot"))
    //    {
    //        other.GetComponent<PlotHelper>().isOccupied = false;
    //    }
    //}
}
