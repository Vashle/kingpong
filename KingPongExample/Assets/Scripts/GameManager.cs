﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    public static GameManager GM;
    public int p1Score, p2Score;

    public PlayerManager playerManager; //we may need to turn this on/off or control it from here.

    //putting the building choosing here in case we want to change it in in-game options.
    //else i would put it in the BuildingManager's start() function, probably.
    public int numOfBuildingsThisGame; //probably going to always be two, but maybe we have the option to have 4 different buildings instead
    public List<int> chosenBuildings;

    //some or all of these should be changable by player:
    [Header("Game Options")]
    public bool losersBall = true; //if true, the ball will start going towards the person who just scored.
    public bool customBuildingOverride; //not sure if we'll need this atm, but maybe.


    public int currentScene;

    private void Awake()
    {
        if (GM == null)
        {
            GM = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }
        currentScene = SceneManager.GetActiveScene().buildIndex;

    }

    // Use this for initialization
    void Start() {
    }

    // Update is called once per frame
    void Update() {
    }

    void RestartScene() //loads a new build of current scene--- does not save any data from scene
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        currentScene = SceneManager.GetActiveScene().buildIndex;

    }

    public void ChooseNewBuildings(List<GameObject> buildns)
    {
        //loop through 
        for (int i = 0; i < numOfBuildingsThisGame; i++)
        {
            chosenBuildings.Add(i);//add it to the list first to 'resize it';
            int rand = Random.Range(0, buildns.Count);

            //!! while loop alert:
            //to make sure the same building isn't chosen twice, check the list for rand.
            //if rand is in there, roll the dice again.
            while (chosenBuildings.Contains(rand))
            {
                rand = Random.Range(0, buildns.Count);
            }
            chosenBuildings[i] = rand;
            print("Building " + rand + ", " + buildns[rand].name + " has been chosen");
        }
    }


}
