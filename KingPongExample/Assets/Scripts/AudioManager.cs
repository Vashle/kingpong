﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {
    public static AudioManager AM;

    public AudioSource bgMusic;

    //a bulky list of common sfx
    public AudioClip[] sfxList;

    private void Awake()
    {
        AM = this;
    }
    // Use this for initialization
    void Start () {
        bgMusic = GetComponent<AudioSource>();
	}


    /// <summary>
    /// send this a clip and it will play at regular volume
    /// </summary>
    /// <param name="whatClip"></param>
    public void PlayClip(AudioClip whatClip)
    {
        //play the clip at the camera's position (for full sound)
        AudioSource.PlayClipAtPoint(whatClip, Camera.main.transform.position);
    }

    /// <summary>
    /// send this a clip and also a volume:
    /// </summary>
    /// <param name="whatClip"></param>
    /// <param name="volume"></param>
    public void PlayClip(AudioClip whatClip, float volume)
    {
        //play the clip at the camera's position (for full sound)
        AudioSource.PlayClipAtPoint(whatClip, Camera.main.transform.position, volume);
    }

    /// <summary>
    /// play a clip from the list of SFX
    /// 0=coinpickup
    /// 
    /// </summary>
    /// <param name="whatSFX"></param>
    public void PlayClip(int whatSFX)
    {
        //play the clip at the camera's position (for full sound)
        AudioSource.PlayClipAtPoint(sfxList[whatSFX], Camera.main.transform.position);
    }

    /// <summary>
    /// remember to reset it!
    /// </summary>
    /// <param name="volume"></param>
    public void SetBGMusic(float volume)
    {
        bgMusic.volume = volume;
    }

}
