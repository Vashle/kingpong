﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingManager : MonoBehaviour {

    public List<GameObject> buildings;
    /**
     * [0]-Normal (Reflects ball)
     * [1]-Spawn (Grave; shoots 'ball' attacks)
     * [2]-Bank (+1 gold/sec ; max y amount of gold generated)
     * [3]-Banana Stand (Player can grab banana & throw; 1 banana per 5 sec)
     * [4]-2nd Ball (Spawns 2nd ball (fast); max 2 balls on stage)
     * [5]-Angel's Blessing (Player is invincible until building destroyed; 1 hp;10 sec cooldown after building)
     * [6]-Devil's Curse (Opponent is stunned until building destroyed; 1 hp; 10 sec cooldown after building)
     */

    // Theoretically, this will be chosen once the player gets into the Game scene (and this GO becomes alive)
    void Start () {
        GameManager.GM.ChooseNewBuildings(buildings);
	}
}
