﻿	using System.Collections.Generic;
	using InControl;
	using UnityEngine;


// This example roughly illustrates the proper way to add multiple playerPaddles from existing
// devices. Notice how InputManager.Devices is not used and no index into it is taken.
// Rather a device references are stored in each player and we use InputManager.OnDeviceDetached
// to know when one is detached.
//
// InputManager.Devices should be considered a pool from which devices may be chosen,
// not a player list. It could contain non-responsive or unsupported controllers, or there could
// be more connected controllers than your game supports, so that isn't a good strategy.
//
// To detect a joining player, we just check the current active device (which is the last
// device to provide input) for a relevant button press, check that it isn't already assigned
// to a player, and then create a new player with it.
//
// NOTE: Due to how Unity handles joysticks, disconnecting a single device will currently cause
// all devices to detach, and the remaining ones to reattach. There is no reliable workaround
// for this issue. As a result, a disconnecting controller essentially resets this example.
// In a more real world scenario, we might keep the playerPaddles around and throw up some UI to let
// users activate controllers and pick their playerPaddles again before resuming.
//
// This example could easily be extended to use bindings. The process would be very similar,
// just creating a new instance of your action set subclass per player and assigning the
// device to its Device property.
//
public class PlayerManager : MonoBehaviour
{
    public GameObject playerPaddlePrefab;
    public GameObject playerPlayerPrefab;

    const int maxPlayers = 4;

    List<Vector3> playerPaddlePositions = new List<Vector3>() {
            new Vector3( -8, 0, 0 ),
            new Vector3( 8, 0, 0 ),
            new Vector3( -1, -1, -10 ),
            new Vector3( 1, -1, -10 ),
        };
    List<Vector3> playerCharacterPositions = new List<Vector3>() {
            new Vector3( -7, 0, 0 ),
            new Vector3( 7, 0, 0 ),
            new Vector3( -1, -1, -10 ),
            new Vector3( 1, -1, -10 ),
        };

    public List<PaddleMove> playerPaddles = new List<PaddleMove>(maxPlayers);
    public List<Player> playerPlayers = new List<Player>(maxPlayers);


    void Start()
    {
        InputManager.OnDeviceDetached += OnDeviceDetached;
        
    }


    void Update()
    {
        var inputDevice = InputManager.ActiveDevice;
        if (JoinButtonWasPressedOnDevice(inputDevice))
        {
            if (ThereIsNoPlayerUsingDevice(inputDevice))
            {
                CreatePaddlePlayer(inputDevice);
                CreatePlayerPlayer(inputDevice);
            }
        }
    }


    bool JoinButtonWasPressedOnDevice(InputDevice inputDevice)
    {
        return inputDevice.Action1.WasPressed || inputDevice.Action2.WasPressed || inputDevice.Action3.WasPressed || inputDevice.Action4.WasPressed;
    }


    PaddleMove FindPaddlePlayerUsingDevice(InputDevice inputDevice)
    {
        var playerCount = playerPaddles.Count;
        for (var i = 0; i < playerCount; i++)
        {
            var player = playerPaddles[i];
            if (player.Device == inputDevice)
            {
                return player;
            }
        }

        return null;
    }

    Player FindPlayerPlayerUsingDevice(InputDevice inputDevice)
    {
        var playerCount = playerPlayers.Count;
        for (var i = 0; i < playerCount; i++)
        {
            var player = playerPlayers[i];
            if (player.Device == inputDevice)
            {
                return player;
            }
        }

        return null;
    }


    bool ThereIsNoPlayerUsingDevice(InputDevice inputDevice)
    {
        return FindPaddlePlayerUsingDevice(inputDevice) == null && FindPlayerPlayerUsingDevice(inputDevice) == null;
    }


    void OnDeviceDetached(InputDevice inputDevice)
    {
        var paddle = FindPaddlePlayerUsingDevice(inputDevice);
        var player = FindPlayerPlayerUsingDevice(inputDevice);

        if (player != null && paddle != null)
        {
            RemovePaddlePlayer(paddle);
            RemovePlayerPlayer(player);
        }
    }


    PaddleMove CreatePaddlePlayer(InputDevice inputDevice)
    {
        if (playerPaddles.Count < maxPlayers)
        {
            // Pop a position off the list. We'll add it back if the player is removed.
            var playerPosition = playerPaddlePositions[0];
            playerPaddlePositions.RemoveAt(0);

            var gameObject = (GameObject)Instantiate(playerPaddlePrefab, playerPosition, Quaternion.Euler(new Vector3(0,0,90)));
            gameObject.transform.parent = GameManager.GM.transform;
            var player = gameObject.GetComponent<PaddleMove>();
            player.Device = inputDevice;
            playerPaddles.Add(player);
            return player;
        }

        return null;
    }

    Player CreatePlayerPlayer(InputDevice inputDevice)
    {
        if(playerPlayers.Count < maxPlayers)
        {
            var playerPosition = playerCharacterPositions[0];
            playerCharacterPositions.RemoveAt(0);

            var gameObject = (GameObject)Instantiate(playerPlayerPrefab, playerPosition, Quaternion.Euler(new Vector3(0, 0, 90)));
            gameObject.transform.parent = GameManager.GM.transform;

            var player = gameObject.GetComponent<Player>();
            player.Device = inputDevice;
            player.playerNum = playerPlayers.Count+1; //assign the player its correct number
            playerPlayers.Add(player);
            UIManager.UIM.EnableJoinElements(player.playerNum, false); //turn off the join text

            return player;
        }
        return null;
    }


    void RemovePaddlePlayer(PaddleMove pad)
    {
        playerPaddlePositions.Insert(0, pad.transform.position);
        playerPaddles.Remove(pad);
        pad.Device = null;
        Destroy(pad.gameObject);
    }

    void RemovePlayerPlayer(Player pad)
    {
        playerCharacterPositions.Insert(0, pad.transform.position);
        playerPlayers.Remove(pad);
        pad.Device = null;
        UIManager.UIM.EnableJoinElements(pad.playerNum, true); //turn off the join text

        Destroy(pad.gameObject);
    }


    //void OnGUI()
    //{
    //    const float h = 22.0f;
    //    var y = 10.0f;

    //    GUI.Label(new Rect(10, y, 300, y + h), "Active playerPaddles: " + playerPaddles.Count + "/" + maxPlayers);
    //    y += h;

    //    if (playerPaddles.Count < maxPlayers)
    //    {
    //        GUI.Label(new Rect(10, y, 300, y + h), "Press a button to join!");
    //        y += h;
    //    }
    //}
}