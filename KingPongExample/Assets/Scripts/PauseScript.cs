﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class PauseScript : MonoBehaviour {

    public bool paused;

    // Use this for initialization
    void Start () {
        paused = false;
	}
	
	// Update is called once per frame
	void Update () {
        var inputDevice = InputManager.ActiveDevice;

        if (!UIManager.UIM.countingDown && !UIManager.UIM.titleCan.activeSelf && inputDevice.CommandWasPressed)
            paused = !paused;
        if (paused)
        {
            Time.timeScale = 0;
            //also, show the pause img:
            if (!UIManager.UIM.pauseImg.enabled)
            {
                UIManager.UIM.pauseImg.enabled = true;

            }
        }
        else
        {
            Time.timeScale = 1;
            if (UIManager.UIM.pauseImg.enabled)
            {
                UIManager.UIM.pauseImg.enabled = false;

            }
        }
	}


}
