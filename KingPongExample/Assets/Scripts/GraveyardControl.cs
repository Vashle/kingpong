﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraveyardControl : Building {
    public float timeToSpawn;
    public GameObject prefabToSpawn;
    public int myPlayer;
    

	// Use this for initialization
	void Start () {
        InvokeRepeating("SpawnSkelly", timeToSpawn, timeToSpawn);
	}

    void SpawnSkelly()
    {
        GameObject go = Instantiate(prefabToSpawn, transform.position, Quaternion.identity);
        go.GetComponent<SimpleMove>().myPlayer = myPlayer;
    }
}
